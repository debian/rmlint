rmlint (2.10.2-0.1) unstable; urgency=medium

  * New upstream version 2.10.2 (Closes: #1043292)
  * Replace Nose with Pytest (Closes: #1018616)
  * xattr needs a new fix (Closes: #1065947)

 -- Alexandre Detiste <tchet@debian.org>  Wed, 19 Feb 2025 20:18:26 +0100

rmlint (2.9.0-2.5) unstable; urgency=high

  * Non-maintainer upload.
  * Add upstream fix for GUI startup failure with recent python3.11.
    (Closes: #1040940)

 -- Adrian Bunk <bunk@debian.org>  Sat, 05 Aug 2023 17:16:05 +0300

rmlint (2.9.0-2.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix error in other packages caused by invalid python package version
    number (cherry-picking upstream patch; closes: #1040179)

 -- Julian Gilbey <jdg@debian.org>  Wed, 05 Jul 2023 09:31:46 +0100

rmlint (2.9.0-2.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix data loss caused by faulty check in generated scripts (Closes: #977410)
    - Use upstream patch

 -- Timo Röhling <timo@gaussglocke.de>  Thu, 15 Apr 2021 23:03:37 +0200

rmlint (2.9.0-2.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS for cannot remove directory. (Closes: #975205)
    - Mention explicitly the file and directory to be cleaned.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Tue, 01 Dec 2020 20:37:43 +0000

rmlint (2.9.0-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix ftbfs with GCC-10. (Closes: #957764)
    - Use upstream patch, thanks to Reiner Herrmann.

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Thu, 20 Aug 2020 20:43:54 +0100

rmlint (2.9.0-2) unstable; urgency=medium

  * Replace glib-2_62.patch with upstream implementation, which
    provides a fallback and hopefully will allow an ia64 build.

 -- Carlos Maddela <e7appew@gmail.com>  Wed, 01 Jan 2020 09:27:25 +1100

rmlint (2.9.0-1) unstable; urgency=medium

  * New upstream release [2.9.0]. (Closes: #928521)
  * Fix incorrect use of findstring function in debian/rules.
  * debian/patches/*:
    - Rebase drops patches already applied upstream: doco-typos.patch,
      fix-nosetests-search.patch and fix-pre-sse4.2.patch.
    - Update reproducible-build.patch to be Python version-agnostic
      so that package may be built with scons 3.1.2-1. (Closes: #947580)
    - Add xattr-fixes.patch cherry-picked from upstream
      which may resolve possible build failures in kfreebsd.
    - Add python3.8.patch to fix syntax warnings and missing metadata
      generated in egg-info.
    - Add glib-2_62.patch to remove deprecated API calls.
  * Drop use of debian/compat file.
  * Drop debian/rmlint-doc.lintian-overrides.
  * Update debian/clean.
  * Indicate compliance with Debian Policy 4.4.1 (no changes required).
  * Fix missing rmlint-gui dependency on python3-gi-cairo
    so that graphics are correctly displayed.
    Thanks to Andres for report. (Closes: #947615)

 -- Carlos Maddela <e7appew@gmail.com>  Tue, 31 Dec 2019 13:22:33 +1100

rmlint (2.8.0-3) unstable; urgency=medium

  * Re-enable SSE4.2 optimisations without causing SIGILL faults.
  * Fix changelog details in debian/upstream/metadata.
  * Update debian/gbp.conf to conform with DEP14 conventions.
  * Suppress maintainer-script-supports-ancient-package-version
    lintian warnings.
  * Update sed rule to substitute local copy of MathJax script.

 -- Carlos Maddela <e7appew@gmail.com>  Mon, 28 Jan 2019 11:31:11 +1100

rmlint (2.8.0-2) unstable; urgency=medium

  * Fix debian/tests/privacy-breach test to account for change
    in document location.
  * Allow debhelper to handle stripping of binaries.
  * Build with Debhelper compat level 12.
  * Remove unnecessary build dependencies for nocheck build profile.
  * Fix builds for hurd, kfreebsd and sparc64 by relaxing build dependency
    on python3-psutil for those architectures. It currently doesn't support
    those architectures and is only required for running tests anyway.
  * Fix nosetests search.
  * Fix program termination by SIGILL signal, by removing sse4 instructions.
  * Update debian/clean list.

 -- Carlos Maddela <e7appew@gmail.com>  Thu, 10 Jan 2019 12:15:02 +1100

rmlint (2.8.0-1) unstable; urgency=medium

  * New upstream release [2.8.0].
  * debian/patches/*:
    - Rebase patches.
    - Drop manpage-typos.patch already applied upstream.
    - Add doco-typos.patch to fix typos in documentation.
    - Drop parameterized-workaround.patch, which is no longer
      necessary.
  * Add dependency on packaged version of Vollkorn desktop font,
    so that its use in HTML docs may be restored.  Thanks to
    Adam Borowski for suggestion.
  * Add debian/gbp.conf.
  * debian/control:
    - Update Vcs-* to salsa.debian.org.
    - Remove unnecessary Testsuite field.
    - Set "Rules-Requires-Root: no".
    - Indicate compliance with Debian Policy 4.3.0.
    - Update build dependencies.
  * Update lintian override to debian-watch-does-not-check-gpg-signature.
  * Add machine-readable upstream metadata.
  * debian/rules:
    - Simplify LFS build rules.
  * Update debian/copyright.
  * Build with Debhelper compat level 11.
  * Link instances of jquery to packaged version.
  * Add maintainer scripts for upgrading to and downgrading from
    this version of rmlint-doc, since it has some symlink changes.

 -- Carlos Maddela <e7appew@gmail.com>  Mon, 31 Dec 2018 23:52:54 +1100

rmlint (2.6.1-1) unstable; urgency=medium

  * New upstream version [2.6.1].
  * Rebase patch queue and drop changes already applied upstream.
  * Indicate compliance with latest standards version [4.0.0].
  * Make 'parameterized' module available to 'nose' tests.
  * Simplify (and hopefully make adaptable to future changes in host
    names and version numbers) the sed rules for patching HTML docs.
  * Fix man page typos.
  * Avoid calling dpkg-parsechangelog directly from debian/rules.
  * Fix new privacy breach issues by disabling the asciinema demo
    and the use of web fonts.
  * Add package test to prevent privacy breach issues in HTML docs.

 -- Carlos Maddela <e7appew@gmail.com>  Mon, 31 Jul 2017 09:44:43 +1000

rmlint (2.4.6-2) unstable; urgency=medium

  * Fix missing dependencies in rmlint-gui package. (Closes: #855255)
  * Rename debian/docs to debian/rmlint.docs for consistency's sake.
  * Fix FTBFS for hurd-i386.
  * Exclude compiled schema from being packaged with rmlint-gui.
  * Fix formatting of unordered list in man page.
  * Fix HTML generated by privacy-breach-workaround.patch.

 -- Carlos Maddela <e7appew@gmail.com>  Fri, 17 Feb 2017 13:35:29 +1100

rmlint (2.4.6-1) unstable; urgency=medium

  * Initial upload to Debian. Thanks to Axel Beckert for getting
    it all started, and to Roger Shimizu for suggested improvements.
    (Closes: #845155)

 -- Carlos Maddela <e7appew@gmail.com>  Tue, 14 Feb 2017 23:11:30 +1100
